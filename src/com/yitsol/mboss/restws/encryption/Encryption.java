
package com.yitsol.mboss.restws.encryption;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;


public class Encryption {
	
	private String encryptionKey = "rzrzerzerz1ttqwd";
	
	/**
	 * This method with encrypt the given string with encryptionKey and generate
	 * secure key
	 * 
	 * @param plainText
	 * @return
	 * @throws Exception
	 */
	
	public String encrypt(String plainText) throws Exception {
	
		Cipher cipher = getCipher(Cipher.ENCRYPT_MODE);
		byte[] encryptedBytes = cipher.doFinal(plainText.getBytes());
		
		return Base64.encodeBase64String(encryptedBytes);
	}
	
	/**
	 * 
	 * @param encrypted
	 * @return
	 * @throws Exception
	 */
	
	public String decrypt(String encrypted) throws Exception {
	
		Cipher cipher = getCipher(Cipher.DECRYPT_MODE);
		byte[] plainBytes = cipher.doFinal(Base64.decodeBase64(encrypted));
		
		return new String(plainBytes);
	}
	
	/**
	 * This method will take int key and return Cipher Object
	 * 
	 * @param cipherMode
	 * @return
	 * @throws Exception
	 */
	
	private Cipher getCipher(int cipherMode) throws Exception {
	
		String encryptionAlgorithm = "AES";
		SecretKeySpec keySpecification = new SecretKeySpec(encryptionKey.getBytes("UTF-8"),
				encryptionAlgorithm);
		Cipher cipher = Cipher.getInstance(encryptionAlgorithm);
		cipher.init(cipherMode, keySpecification);
		
		return cipher;
	}
	
}
