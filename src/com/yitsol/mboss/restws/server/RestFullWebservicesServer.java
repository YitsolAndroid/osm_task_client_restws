
package com.yitsol.mboss.restws.server;

/**
 * 
 * @author Sahil Nokhwal
 * 
 * @FileName: RestFullWebservicesServer.java
 * 
 * @version: 1.0.0.0.0
 * 
 * @Descrption This class is use for accessing Restful
 *             Webservices
 *
 *Copyright and all rights reserved.
 *
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.sql.SQLRecoverableException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import javax.xml.soap.SOAPException;

import oracle.net.ns.NetException;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import com.yitsol.mboss.restws.client.FindOrderSoapClient;
import com.yitsol.mboss.restws.client.WebserviceHTTPClient;
import com.yitsol.mboss.restws.exception.GenericAllException;
import com.yitsol.mboss.restws.util.RestfulJsonUtil;


@Path("/orderServices")
public class RestFullWebservicesServer {
	
	final static Logger logger = Logger.getLogger(RestFullWebservicesServer.class);
	
	private JSONObject jsonOutputObject;
	private String username;
	private String password;
	private String orderId;
	private WebserviceHTTPClient wsHttpClient = null;
	private JSONObject jsonInputObject;
	
	@Context
	ServletContext ctx;
	@Context
	ContainerRequestContext request;
	
	// setter method for Username, Password, OrderId
	/**
	 * 
	 * @param jsonInputObject
	 * @throws JSONException
	 */
	private void setter(JSONObject jsonInputObject) throws JSONException {
	
		username = (String) request.getProperty("username"); //$NON-NLS-1$
		password = (String) request.getProperty("password"); //$NON-NLS-1$
		orderId = jsonInputObject.getString("orderId"); //$NON-NLS-1$
	}
	
	/**
	 * 
	 * @param incommingJsonObj
	 * @param ctx
	 * @return
	 * @throws Exception
	 */
	// getOrder Restful WebService
	@POST
	@Path("/getOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public String getOrder(InputStream incommingJsonObj) {
	
		logger.info(Messages.getString("ExceptionMsg3")); //$NON-NLS-1$
		logger.info(Messages.getString("ExceptionMsg4")); //$NON-NLS-1$
		String soapMessageAsString = null;
		
		try {
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			
			String remark = jsonInputObject.getString("remark"); //$NON-NLS-1$
			// here we are getting response from soap webservices
			setter(jsonInputObject);
			wsHttpClient = new WebserviceHTTPClient();
			soapMessageAsString = wsHttpClient.getOrder(username, password, orderId, remark, ctx);
			logger.info(Messages.getString("ExceptionMsg6")); //$NON-NLS-1$
		}
		catch (JSONException e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg7"); //$NON-NLS-1$
		}
		
		catch (GenericAllException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg8"); //$NON-NLS-1$
		}
		
		catch (SOAPException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg9"); //$NON-NLS-1$
		}
		catch (Exception e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg10"); //$NON-NLS-1$
		}
		
		finally {
			try {
				incommingJsonObj.close();
			}
			catch (IOException e) {
				
				logger.error(e);
			}
		}
		
		return RestfulJsonUtil.checkSoapMessage(soapMessageAsString);
	}
	
	// updateOrderUsingDataChange Restful WebService
	
	@POST
	@Path("/updateOrderUsingDataChange")
	@Produces("application/json")
	@Consumes("application/json")
	public String updateOrderUsingDataChange(InputStream incommingJsonObj) {
	
		// UpdateType updateType = null;
		String soapMessageAsString = null;
		String responsedata = null;
		try {
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			// String updateTypeString =
			// jsonInputObject.getString("updateType");
			String startOrder = jsonInputObject.getString("startOrder"); //$NON-NLS-1$
			String dataChange = jsonInputObject.getString("dataChange"); //$NON-NLS-1$
			
			// here we are getting response from soap webservices
			
			wsHttpClient = new WebserviceHTTPClient();
			soapMessageAsString = wsHttpClient.updateOrderUsingDataChange(username, password,
					orderId, ctx, startOrder, dataChange);
			
		}
		catch (JSONException e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg7"); //$NON-NLS-1$
		}
		
		catch (GenericAllException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg8"); //$NON-NLS-1$
		}
		
		catch (SOAPException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg9"); //$NON-NLS-1$
		}
		catch (Exception e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg10"); //$NON-NLS-1$
		} finally {
			try {
				incommingJsonObj.close();
			}
			catch (IOException e) {
				
				logger.error(e);
			}
		}
		
		return RestfulJsonUtil.checkSoapMessage(soapMessageAsString);
	}
	
	// updateOrderUsingUpdatedNodes Restful WebService
	
	@POST
	@Path("/updateOrderUsingUpdatedNodes")
	@Produces("application/json")
	@Consumes("application/json")
	public String updateOrderUsingUpdatedNodes(InputStream incommingJsonObj) {
	
		String responsedata = null;
		String soapMessageAsString = null;
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			// String updateTypeString =
			// jsonInputObject.getString("updateType");
			String payment = jsonInputObject.getString("payment"); //$NON-NLS-1$
			String payEntire = jsonInputObject.getString("payEntire"); //$NON-NLS-1$
			// UpdateType updateType = null;
			// for (UpdateType ut : UpdateType.values()) {
			// if (updateTypeString.equals(UpdateType.values().toString())) {
			// updateType = UpdateType.valueOf(updateTypeString);
			// break;
			// }
			// }
			
			// here we are getting response from soap webservices
			wsHttpClient = new WebserviceHTTPClient();
			soapMessageAsString = wsHttpClient.updateOrderUsingUpdatedNodes(username, password,
					orderId, ctx, payment, payEntire);
		}
		catch (JSONException e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg7"); //$NON-NLS-1$
		}
		
		catch (GenericAllException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg8"); //$NON-NLS-1$
		}
		
		catch (SOAPException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg9"); //$NON-NLS-1$
		}
		catch (Exception e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg10"); //$NON-NLS-1$
		} finally {
			try {
				incommingJsonObj.close();
			}
			catch (IOException e) {
				
				logger.error(e);
			}
		}
		
		return RestfulJsonUtil.checkSoapMessage(soapMessageAsString);
	}
	
	// updateOrderUsingUpdatedOrder Restful WebService
	
	@POST
	@Path("/updateOrderUsingUpdatedOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public String updateOrderUsingUpdatedOrder(InputStream incommingJsonObj) {
	
		String responsedata = null;
		String soapMessageAsString = null;
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			// String updateTypeString =
			// jsonInputObject.getString("updateType");
			String xml = jsonInputObject.getString("xml"); //$NON-NLS-1$
			
			// UpdateType updateType = null;
			// for (UpdateType ut : UpdateType.values()) {
			// if (updateTypeString.equals(UpdateType.values().toString())) {
			// updateType = UpdateType.valueOf(updateTypeString);
			// break;
			// }
			// }
			
			// here we are getting response from soap webservices
			wsHttpClient = new WebserviceHTTPClient();
			soapMessageAsString = wsHttpClient.updateOrderUsingUpdatedOrder(username, password,
					orderId, ctx, xml);
		}
		catch (JSONException e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg7"); //$NON-NLS-1$
		}
		
		catch (GenericAllException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg8"); //$NON-NLS-1$
		}
		
		catch (SOAPException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg9"); //$NON-NLS-1$
		}
		catch (Exception e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg10"); //$NON-NLS-1$
		} finally {
			try {
				incommingJsonObj.close();
			}
			catch (IOException e) {
				
				logger.error(e);
			}
		}
		return RestfulJsonUtil.checkSoapMessage(soapMessageAsString);
	}
	
	// resumeOrder Restful WebService
	@POST
	@Path("/resumeOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public String resumeOrder(InputStream incommingJsonObj) {
	
		String soapMessageAsString = null;
		String responsedata = null;
		try {
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			String reason = jsonInputObject.getString("reason"); //$NON-NLS-1$
			// here we are getting response from soap webservices
			wsHttpClient = new WebserviceHTTPClient();
			soapMessageAsString = wsHttpClient
					.resumeOrder(username, password, orderId, reason, ctx);
			logger.info(soapMessageAsString);
		}
		catch (JSONException e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg7"); //$NON-NLS-1$
		}
		
		catch (SOAPException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg11"); //$NON-NLS-1$
		}
		
		catch (Exception e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg10"); //$NON-NLS-1$
		} finally {
			try {
				incommingJsonObj.close();
			}
			catch (IOException e) {
				
				logger.error(e);
			}
		}
		return RestfulJsonUtil.checkSoapMessage(soapMessageAsString);
	}
	
	// suspendOrder Restful WebService
	
	@POST
	@Path("/suspendOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public String suspendOrder(InputStream incommingJsonObj) {
	
		String soapMessageAsString = null;
		String responsedata = null;
		try {
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			String reason = jsonInputObject.getString("reason"); //$NON-NLS-1$
			// here we are getting response from soap webservices
			wsHttpClient = new WebserviceHTTPClient();
			soapMessageAsString = wsHttpClient.suspendOrder(username, password, orderId, reason,
					ctx);
		}
		catch (JSONException e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg7"); //$NON-NLS-1$
		}
		
		catch (SOAPException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg11"); //$NON-NLS-1$
		}
		
		catch (Exception e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg10"); //$NON-NLS-1$
		} finally {
			try {
				incommingJsonObj.close();
			}
			catch (IOException e) {
				
				logger.error(e);
			}
		}
		return RestfulJsonUtil.checkSoapMessage(soapMessageAsString);
	}
	
	// cancelOrder Restful WebService
	
	@POST
	@Path("/cancelOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public String cancelOrder(InputStream incommingJsonObj) {
	
		String soapMessageAsString = null;
		String responsedata = null;
		try {
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			String reason = jsonInputObject.getString("reason"); //$NON-NLS-1$
			// here we are getting response from soap webservices
			wsHttpClient = new WebserviceHTTPClient();
			soapMessageAsString = wsHttpClient
					.cancelOrder(username, password, orderId, reason, ctx);
		}
		catch (JSONException e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg7"); //$NON-NLS-1$
		}
		
		catch (SOAPException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg11"); //$NON-NLS-1$
		}
		
		catch (Exception e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg12"); //$NON-NLS-1$
		} finally {
			try {
				incommingJsonObj.close();
			}
			catch (IOException e) {
				
				logger.error(e);
			}
		}
		return RestfulJsonUtil.checkSoapMessage(soapMessageAsString);
	}
	
	// FailOrder
	@POST
	@Path("/failOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public String failOrder(InputStream incommingJsonObj) {
	
		String soapMessageAsString = null;
		String responsedata = null;
		try {
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			
			// here we are getting response from soap webservices
			wsHttpClient = new WebserviceHTTPClient();
			soapMessageAsString = wsHttpClient.failOrder(username, password, orderId, ctx);
		}
		catch (JSONException e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg7"); //$NON-NLS-1$
		}
		
		catch (SOAPException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg9"); //$NON-NLS-1$
		}
		
		catch (Exception e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg10"); //$NON-NLS-1$
		} finally {
			try {
				incommingJsonObj.close();
			}
			catch (IOException e) {
				
				logger.error(e);
			}
		}
		return RestfulJsonUtil.checkSoapMessage(soapMessageAsString);
	}
	
	// FindOrder
	@POST
	@Path("/findOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public String findOrder(InputStream incommingJsonObj) {
	
		String soapMessageAsString = null;
		String responsedata = null;
		try {
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			username = jsonInputObject.getString("username"); //$NON-NLS-1$
			password = jsonInputObject.getString("password"); //$NON-NLS-1$
			
			// here we are getting response from soap webservices
			wsHttpClient = new WebserviceHTTPClient();
			String xml = jsonInputObject.getString("xml"); //$NON-NLS-1$
			FindOrderSoapClient findOrderClient = new FindOrderSoapClient();
			soapMessageAsString = findOrderClient.findOrder(username, password, ctx, xml);
		}
		catch (JSONException e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg7"); //$NON-NLS-1$
		}
		
		catch (SOAPException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg9"); //$NON-NLS-1$
		}
		
		catch (Exception e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg10"); //$NON-NLS-1$
		} finally {
			try {
				incommingJsonObj.close();
			}
			catch (IOException e) {
				
				logger.error(e);
			}
		}
		return RestfulJsonUtil.checkSoapMessage(soapMessageAsString);
	}
	
	// ResolveFailure
	@POST
	@Path("/resolveFailure")
	@Produces("application/json")
	@Consumes("application/json")
	public String resolveFailure(InputStream incommingJsonObj) {
	
		String responsedata = null;
		String soapMessageAsString = null;
		try {
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			String reason = jsonInputObject.getString("reason"); //$NON-NLS-1$
			
			// here we are getting response from soap webservices
			wsHttpClient = new WebserviceHTTPClient();
			soapMessageAsString = wsHttpClient.resolveFailure(username, password, orderId, reason,
					ctx);
		}
		catch (JSONException e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg7"); //$NON-NLS-1$
		}
		
		catch (SOAPException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg9"); //$NON-NLS-1$
		}
		
		catch (Exception e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg10"); //$NON-NLS-1$
		} finally {
			try {
				incommingJsonObj.close();
			}
			catch (IOException e) {
				
				logger.error(e);
			}
		}
		return RestfulJsonUtil.checkSoapMessage(soapMessageAsString);
	}
	
	// AbortOrder
	@POST
	@Path("/abortOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public String abortOrder(InputStream incommingJsonObj) {
	
		String soapMessageAsString = null;
		String responsedata = null;
		try {
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			String reason = jsonInputObject.getString("reason"); //$NON-NLS-1$
			
			// here we are getting response from soap webservices
			wsHttpClient = new WebserviceHTTPClient();
			soapMessageAsString = wsHttpClient.abortOrder(username, password, orderId, reason, ctx);
		}
		catch (JSONException e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg7"); //$NON-NLS-1$
		}
		
		catch (SOAPException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg9"); //$NON-NLS-1$
		}
		
		catch (Exception e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg10"); //$NON-NLS-1$
		} finally {
			try {
				incommingJsonObj.close();
			}
			catch (IOException e) {
				
				logger.error(e);
			}
		}
		return RestfulJsonUtil.checkSoapMessage(soapMessageAsString);
	}
	
}
