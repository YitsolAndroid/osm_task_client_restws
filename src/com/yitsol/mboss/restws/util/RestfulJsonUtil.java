
package com.yitsol.mboss.restws.util;

/**
 * 
 * @author Sahil Nokhwal
 * @return
 * @throws Exception
 *
 *
 */

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;

import com.yitsol.mboss.restws.client.WebserviceHTTPClient;
import com.yitsol.mboss.restws.server.RestFullWebservicesServer;


public class RestfulJsonUtil {
	
	public static WebserviceHTTPClient wsHttpClient = null;
	final static Logger logger = Logger.getLogger(RestFullWebservicesServer.class);
	private static JSONObject jsonOutputObject;
	
	/**
	 * 
	 * @param incommingJsonObj
	 * @return
	 * @throws Exception
	 */
	public static String convertInputDataToJsonString(InputStream incommingJsonObj) {
	
		StringBuilder inputDataAsStringBuilder = null;
		try {
			
			inputDataAsStringBuilder = new StringBuilder();
			
			BufferedReader in = new BufferedReader(new InputStreamReader(incommingJsonObj));
			String line = null;
			while ((line = in.readLine()) != null) {
				inputDataAsStringBuilder.append(line);
			}
			logger.info("Data Recieved:");
			
		}
		catch (Exception e) {
			logger.info("Error Parsing: -");
			return "\"status\":\"500 Exception Arised while converting input Data into Json\"";
		}
		
		return inputDataAsStringBuilder.toString();
		
	}
	
	/**
	 * 
	 * @param soapMessageAsString
	 * @return
	 * 
	 */
	public static String convertSoapMessageToJsonString(String soapMessageAsString) {
	
		try {
			// Converting Soap Message to Json Object and returning.
			jsonOutputObject = XML.toJSONObject(soapMessageAsString);
		}
		catch (Exception e) {
			return "\"status\":\"500 Exception Arised while converting soap message to Json\"";
		}
		
		return jsonOutputObject.toString();
	}
	
	/**
	 * 
	 * @param soapMessageAsString
	 * @return
	 * 
	 */
	public static String checkSoapMessage(String soapMessageAsString) {
	
		String responsedata = null;
		try {
			if (soapMessageAsString == null) {
				logger.info(soapMessageAsString);
				return "{\"status\":\"204 No Output Data\"}";
			}
			responsedata = RestfulJsonUtil.convertSoapMessageToJsonString(soapMessageAsString);
		}
		catch (Exception e) {
			logger.error(e);
			return "{\"status\":\"500 Exception Arised, No Output Data\"}";
		}
		
		return responsedata;
		
	}
	
}
