
package com.yitsol.mboss.restws.client;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;

import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.yitsol.mboss.restws.client.model.OrderId;
import com.yitsol.mboss.restws.client.model.TaskClientProperties;
import com.yitsol.mboss.restws.exception.GenericAllException;


public class FindOrderSoapClient {
	
	final static Logger logger = Logger.getLogger(FindOrderSoapClient.class);
	public static TaskClientProperties properties = new TaskClientProperties();
	private static MessageFactory messageFactory;
	private static SOAPConnectionFactory factory;
	private static SOAPConnection connection;
	
	public FindOrderSoapClient ( ) {
	
		try {
			
			factory = SOAPConnectionFactory.newInstance();
			connection = factory.createConnection();
			messageFactory = MessageFactory.newInstance();
			
		}
		catch (UnsupportedOperationException e) {
			
			e.printStackTrace();
			
		}
		catch (SOAPException e) {
			
			e.printStackTrace();
			
		}
		
	}
	
	/**
	 * This Method will create a soap call to Osm Server to Perform Find Order
	 * 
	 * @param username
	 * @param password
	 * @param orderId
	 * @param ctx
	 * @return
	 * @throws SOAPException
	 * @throws Exception
	 */
	
	public String findOrder(String username, String password, ServletContext ctx, String xmlText)
			throws SOAPException {
	
		JAXBContext jaxbContext = null;
		OrderId order = null;
		logger.info("XML:" + xmlText);
		try {
			
			jaxbContext = JAXBContext
					.newInstance(OrderId.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(xmlText.trim());
			order = (OrderId) unmarshaller.unmarshal(reader);
			
		}
		catch (JAXBException e) {
			
			e.printStackTrace();
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"XML File   not found!\"}");
			
		}
		
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		builderFactory.setNamespaceAware(true);
		InputStream stream = new ByteArrayInputStream(xmlText.getBytes());
		try {
			
			Document doc = builderFactory.newDocumentBuilder().parse(stream);
			
		}
		catch (SAXException | IOException | ParserConfigurationException e1) {
			
			e1.printStackTrace();
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"XML File   not found!\"}");
		}
		
		SOAPMessage message = messageFactory.createMessage();
		message.getMimeHeaders().setHeader("SOAPAction", properties.getWs() + "/FindOrder");
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(new File(ctx.getResource(properties.getFIND_ORDER())
					.getPath()));
		}
		catch (FileNotFoundException | MalformedURLException e) {
			e.printStackTrace();
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"XMl File   not found!\"}");
			
		}
		WebserviceHTTPClientUtil.setMessageContent(message, fis);
		// SOAPBodyElement updatedOrder =
		// message.getSOAPBody().addDocument(JAXBElementToDomElement(person));
		SOAPElement selectByElement = (SOAPElement) message.getSOAPBody()
				.getElementsByTagNameNS(properties.getWs(), "SelectBy").item(0);
		
		// selectByElement.addChildElement(updatedOrder);
		SOAPFactory soapFactory = SOAPFactory.newInstance();
		if (!order.orderId.isEmpty()) {
			Name bodyName = soapFactory.createName("OrderId", "ord", null);
			SOAPElement orderId = (SOAPElement) message.getSOAPBody().addBodyElement(bodyName)
					.addTextNode(order.orderId);
			selectByElement.addChildElement(orderId);
		}
		
		if (!order.targetOrderSatate.isEmpty()) {
			
			Name targetorderName = soapFactory.createName("TargetOrderState", "ord", null);
			SOAPElement stateElement = (SOAPElement) message.getSOAPBody()
					.addBodyElement(targetorderName).addTextNode(order.targetOrderSatate);
			selectByElement.addChildElement(stateElement);
		}
		
		if (!order.reference.isEmpty()) {
			
			Name referenceName = soapFactory.createName("TargetOrderState", "ord", null);
			SOAPElement stateElement = (SOAPElement) message.getSOAPBody()
					.addBodyElement(referenceName).addTextNode(order.reference);
			selectByElement.addChildElement(stateElement);
		}
		
		if (!order.orderState.isEmpty()) {
			
			Name stateName = soapFactory.createName("OrderState", "ord", null);
			SOAPElement stateElement = (SOAPElement) message.getSOAPBody()
					.addBodyElement(stateName).addTextNode(order.orderState);
			selectByElement.addChildElement(stateElement);
		}
		
		if (!order.cartridge.name.isEmpty() || !order.cartridge.version.isEmpty()) {
			
			Name cartridgeName = soapFactory.createName("Cartridge", "ord", null);
			SOAPElement cartridgeElement = (SOAPElement) message.getSOAPBody().addBodyElement(
					cartridgeName);
			
			if (!order.cartridge.name.isEmpty()) {
				Name name = soapFactory.createName("Name", "ord", null);
				SOAPElement nameElement = (SOAPElement) message.getSOAPBody().addBodyElement(name)
						.addTextNode(order.cartridge.name);
				cartridgeElement.addChildElement(nameElement);
			}
			if (!order.cartridge.version.isEmpty()) {
				
				Name versionName = soapFactory.createName("Version", "ord", null);
				SOAPElement versionElement = (SOAPElement) message.getSOAPBody()
						.addBodyElement(versionName).addTextNode(order.cartridge.version);
				cartridgeElement.addChildElement(versionElement);
			}
			selectByElement.addChildElement(cartridgeElement);
		}
		
		if (!order.orderType.isEmpty()) {
			
			Name orderTypeName = soapFactory.createName("OrderType", "ord", null);
			SOAPElement stateElement = (SOAPElement) message.getSOAPBody()
					.addBodyElement(orderTypeName).addTextNode(order.orderType);
			selectByElement.addChildElement(stateElement);
		}
		
		if (!order.orderSource.isEmpty()) {
			
			Name orderSourceName = soapFactory.createName("OrderSource", "ord", null);
			SOAPElement stateElement = (SOAPElement) message.getSOAPBody()
					.addBodyElement(orderSourceName).addTextNode(order.orderSource);
			selectByElement.addChildElement(stateElement);
		}
		
		findOrderData(order, message, selectByElement, soapFactory);
		
		if (!order.priority.isEmpty()) {
			
			Name priorityeName = soapFactory.createName("Priority", "ord", null);
			SOAPElement priorityElement = (SOAPElement) message.getSOAPBody()
					.addBodyElement(priorityeName).addTextNode(order.orderType);
			selectByElement.addChildElement(priorityElement);
		}
		
		findOrderField(order, message, selectByElement, soapFactory);
		
		if (!order.maxorders.isEmpty()) {
			
			Name maxOrdersName = soapFactory.createName("MaxOrders", "ord", null);
			SOAPElement maxOrdersElement = (SOAPElement) message.getSOAPBody()
					.addBodyElement(maxOrdersName).addTextNode(order.orderType);
			selectByElement.addChildElement(maxOrdersName);
		}
		
		SOAPElement findOrderElement = (SOAPElement) message.getSOAPBody()
				.getElementsByTagNameNS(properties.getWs(), "FindOrder").item(0);
		
		if (!order.orderField.header.isEmpty() || !order.orderField.sortBy.isEmpty()) {
			Name orderByeName = soapFactory.createName("OrderBy", "ord", null);
			SOAPElement orderByElement = (SOAPElement) message.getSOAPBody().addBodyElement(
					orderByeName);
			Name orderFieldName = soapFactory.createName("OrderField", "ord", null);
			SOAPElement orderFieldElement = (SOAPElement) message.getSOAPBody().addBodyElement(
					orderFieldName);
			if (!order.orderField.header.isEmpty()) {
				Name header = soapFactory.createName("Header", "ord", null);
				SOAPElement todateElement = (SOAPElement) message.getSOAPBody()
						.addBodyElement(header).addTextNode(order.orderField.header);
				orderFieldElement.addChildElement(todateElement);
			}
			if (!order.orderField.sortBy.isEmpty()) {
				Name sortByName = soapFactory.createName("SortBy", "ord", null);
				SOAPElement fromDateElement = (SOAPElement) message.getSOAPBody()
						.addBodyElement(sortByName).addTextNode(order.orderField.sortBy);
				orderFieldElement.addChildElement(fromDateElement);
			}
			findOrderElement.addChildElement(orderFieldElement);
		}
		
		message.saveChanges();
		
		DOMSource source = new DOMSource(message.getSOAPBody());
		StringWriter stringResult = new StringWriter();
		try {
			TransformerFactory.newInstance().newTransformer()
					.transform(source, new StreamResult(stringResult));
		}
		catch (TransformerFactoryConfigurationError | TransformerException e) {
			e.printStackTrace();
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"XMl File   not found!\"}");
			
		}
		String messBody = stringResult.toString();
		logger.info("soap:" + messBody);
		addWSSecurityHeader(message, username, password);
		String soapmessage = WebserviceHTTPClientUtil.soapCall(message, connection,
				properties.getEndpoint());
		return soapmessage;
		
	}
	
	private void findOrderField(OrderId order, SOAPMessage message, SOAPElement selectByElement,
			SOAPFactory soapFactory) throws SOAPException {
	
		if (!order.field.path.isEmpty()) {
			Name fieldName = soapFactory.createName("Field", "ord", null);
			SOAPElement fieldElement = (SOAPElement) message.getSOAPBody()
					.addBodyElement(fieldName);
			if (!order.field.cartridge.name.isEmpty() || !order.field.cartridge.version.isEmpty()) {
				
				Name cartridgeName = soapFactory.createName("Cartridge", "ord", null);
				SOAPElement cartridgeElement = (SOAPElement) message.getSOAPBody().addBodyElement(
						cartridgeName);
				
				if (!order.field.cartridge.name.isEmpty()) {
					Name name = soapFactory.createName("Name", "ord", null);
					SOAPElement nameElement = (SOAPElement) message.getSOAPBody()
							.addBodyElement(name).addTextNode(order.field.cartridge.name);
					cartridgeElement.addChildElement(nameElement);
				}
				if (!order.field.cartridge.version.isEmpty()) {
					Name versionName = soapFactory.createName("Version", "ord", null);
					SOAPElement versionElement = (SOAPElement) message.getSOAPBody()
							.addBodyElement(versionName).addTextNode(order.field.cartridge.version);
					cartridgeElement.addChildElement(versionElement);
				}
				fieldElement.addChildElement(cartridgeElement);
			}
			Name pathName = soapFactory.createName("Path", "ord", null);
			SOAPElement pathElement = (SOAPElement) message.getSOAPBody().addBodyElement(pathName)
					.addTextNode(order.orderState);
			fieldElement.addChildElement(pathElement);
			
			if (!order.field.equalsto.isEmpty()) {
				Name equalstoName = soapFactory.createName("EqualTo", "ord", null);
				SOAPElement equalstoElement = (SOAPElement) message.getSOAPBody()
						.addBodyElement(equalstoName).addTextNode(order.field.equalsto);
				fieldElement.addChildElement(equalstoElement);
			}
			if (!order.field.to.isEmpty()) {
				Name toName = soapFactory.createName("To", "ord", null);
				SOAPElement toElement = (SOAPElement) message.getSOAPBody().addBodyElement(toName)
						.addTextNode(order.field.to);
				fieldElement.addChildElement(toElement);
			}
			if (!order.field.from.isEmpty()) {
				Name fromName = soapFactory.createName("From", "ord", null);
				SOAPElement fromElement = (SOAPElement) message.getSOAPBody()
						.addBodyElement(fromName).addTextNode(order.field.from);
				fieldElement.addChildElement(fromElement);
			}
			selectByElement.addChildElement(fieldElement);
		}
	}
	
	private void findOrderData(OrderId order, SOAPMessage message, SOAPElement selectByElement,
			SOAPFactory soapFactory) throws SOAPException {
	
		if (!order.createdDate.toDate.isEmpty() || !order.createdDate.fromDate.isEmpty()) {
			Name createName = soapFactory.createName("CreatedDate", "ord", null);
			SOAPElement create = (SOAPElement) message.getSOAPBody().addBodyElement(createName);
			if (order.createdDate.toDate != null) {
				Name bodyName = soapFactory.createName("ToDate", "ord", null);
				SOAPElement orderId = (SOAPElement) message.getSOAPBody().addBodyElement(bodyName)
						.addTextNode(order.createdDate.toDate);
				create.addChildElement(orderId);
			}
			if (!order.createdDate.fromDate.isEmpty()) {
				Name bodyName = soapFactory.createName("FromDate", "ord", null);
				SOAPElement orderId = (SOAPElement) message.getSOAPBody().addBodyElement(bodyName)
						.addTextNode(order.createdDate.fromDate);
				create.addChildElement(orderId);
			}
			selectByElement.addChildElement(create);
		}
		if (!order.completeddate.toDate.isEmpty() || !order.completeddate.fromDate.isEmpty()) {
			Name cmpdName = soapFactory.createName("CompletedDate", "ord", null);
			SOAPElement cmpdElement = (SOAPElement) message.getSOAPBody().addBodyElement(cmpdName);
			if (!order.completeddate.toDate.isEmpty()) {
				Name bodyName = soapFactory.createName("ToDate", "ord", null);
				SOAPElement orderId = (SOAPElement) message.getSOAPBody().addBodyElement(bodyName)
						.addTextNode(order.completeddate.toDate);
				cmpdElement.addChildElement(orderId);
			}
			if (!order.completeddate.fromDate.isEmpty()) {
				Name bodyName = soapFactory.createName("FromDate", "ord", null);
				SOAPElement orderId = (SOAPElement) message.getSOAPBody().addBodyElement(bodyName)
						.addTextNode(order.completeddate.fromDate);
				cmpdElement.addChildElement(orderId);
			}
			selectByElement.addChildElement(cmpdElement);
		}
		if (!order.expectedDuration.toDuration.isEmpty()
				|| !order.expectedDuration.fromDuration.isEmpty()) {
			Name expectedDurantion = soapFactory.createName("ExpectedDuration", "ord", null);
			SOAPElement expectedDurantionElement = (SOAPElement) message.getSOAPBody()
					.addBodyElement(expectedDurantion);
			if (order.expectedDuration.fromDuration != null) {
				Name fromDuration = soapFactory.createName("FromDuration", "ord", null);
				SOAPElement fromDurationElement = (SOAPElement) message.getSOAPBody()
						.addBodyElement(fromDuration)
						.addTextNode(order.expectedDuration.fromDuration);
				expectedDurantionElement.addChildElement(fromDuration);
			}
			if (!order.expectedDuration.toDuration.isEmpty()) {
				Name toDurationName = soapFactory.createName("ToDuration", "ord", null);
				SOAPElement toDurationElement = (SOAPElement) message.getSOAPBody()
						.addBodyElement(toDurationName)
						.addTextNode(order.expectedDuration.toDuration);
				expectedDurantionElement.addChildElement(toDurationName);
			}
			selectByElement.addChildElement(expectedDurantionElement);
		}
		if (!order.expectedStartDate.toDate.isEmpty()
				|| !order.expectedStartDate.fromDate.isEmpty()) {
			Name expectedStartDateName = soapFactory.createName("ExpectedStartDate", "ord", null);
			SOAPElement expectedStartDate = (SOAPElement) message.getSOAPBody().addBodyElement(
					expectedStartDateName);
			if (!order.expectedStartDate.toDate.isEmpty()) {
				Name todate = soapFactory.createName("ToDate", "ord", null);
				SOAPElement todateElement = (SOAPElement) message.getSOAPBody()
						.addBodyElement(todate).addTextNode(order.expectedStartDate.toDate);
				expectedStartDate.addChildElement(todateElement);
			}
			if (!order.expectedStartDate.fromDate.isEmpty()) {
				Name fromDate = soapFactory.createName("FromDate", "ord", null);
				SOAPElement fromDateElement = (SOAPElement) message.getSOAPBody()
						.addBodyElement(fromDate).addTextNode(order.expectedStartDate.fromDate);
				expectedStartDate.addChildElement(fromDateElement);
			}
			selectByElement.addChildElement(expectedStartDate);
		}
		
		if (!order.expectedOrderCompletionDate.toDate.isEmpty()
				|| !order.expectedOrderCompletionDate.fromDate.isEmpty()) {
			
			Name expectedOrderCompletionDateName = soapFactory.createName(
					"ExpectedOrderCompletionDate", "ord", null);
			SOAPElement expectedOrderCompletionDateElement = (SOAPElement) message.getSOAPBody()
					.addBodyElement(expectedOrderCompletionDateName);
			if (!order.expectedOrderCompletionDate.toDate.isEmpty()) {
				
				Name todate = soapFactory.createName("ToDate", "ord", null);
				SOAPElement todateElement = (SOAPElement) message.getSOAPBody()
						.addBodyElement(todate)
						.addTextNode(order.expectedOrderCompletionDate.toDate);
				expectedOrderCompletionDateElement.addChildElement(todateElement);
			}
			if (!order.expectedOrderCompletionDate.fromDate.isEmpty()) {
				
				Name fromDate = soapFactory.createName("FromDate", "ord", null);
				SOAPElement fromDateElement = (SOAPElement) message.getSOAPBody()
						.addBodyElement(fromDate)
						.addTextNode(order.expectedOrderCompletionDate.fromDate);
				expectedOrderCompletionDateElement.addChildElement(fromDateElement);
			}
			selectByElement.addChildElement(expectedOrderCompletionDateElement);
		}
		if (!order.requestedDeliveryDate.toDate.isEmpty()
				|| !order.requestedDeliveryDate.fromDate.isEmpty()) {
			Name requestedDeliveryDateName = soapFactory.createName("RequestedDeliveryDate", "ord",
					null);
			SOAPElement requestedDeliveryDateElement = (SOAPElement) message.getSOAPBody()
					.addBodyElement(requestedDeliveryDateName);
			if (!order.requestedDeliveryDate.toDate.isEmpty()) {
				Name todate = soapFactory.createName("ToDate", "ord", null);
				SOAPElement todateElement = (SOAPElement) message.getSOAPBody()
						.addBodyElement(todate)
						.addTextNode(order.expectedOrderCompletionDate.toDate);
				requestedDeliveryDateElement.addChildElement(todateElement);
			}
			if (!order.requestedDeliveryDate.fromDate.isEmpty()) {
				Name fromDate = soapFactory.createName("FromDate", "ord", null);
				SOAPElement fromDateElement = (SOAPElement) message.getSOAPBody()
						.addBodyElement(fromDate)
						.addTextNode(order.expectedOrderCompletionDate.fromDate);
				requestedDeliveryDateElement.addChildElement(fromDateElement);
			}
			selectByElement.addChildElement(requestedDeliveryDateElement);
		}
	}
	
	/**
	 * This Method will add Security Header to Soap Message
	 * 
	 * @param message
	 * @param username
	 * @param password
	 * @throws SOAPException
	 * @throws Exception
	 */
	private void addWSSecurityHeader(SOAPMessage message, String username, String password)
			throws SOAPException {
	
		String temp = username + ":" + password;
		String authorization = new String(Base64.encodeBase64(temp.getBytes()));
		MimeHeaders hd = message.getMimeHeaders();
		hd.addHeader("Authorization", "Basic " + authorization);
		
		SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
		SOAPHeader header = message.getSOAPHeader();
		Name name = envelope.createName("Security", "wsse", properties.getWsse());
		SOAPElement security = header.addChildElement(name);
		name = envelope.createName("mustUnderstand", "soapenv", properties.getSoapenv());
		security.addAttribute(name, "1");
		
		name = envelope.createName("UsernameToken", "wsse", properties.getWsse());
		SOAPElement usernameToken = security.addChildElement(name);
		name = envelope.createName("Id", "wsu", properties.getWsu());
		usernameToken.addAttribute(name, "UsernameToken-4799946");
		
		name = envelope.createName("Username", "wsse", properties.getWsse());
		SOAPElement usernameElement = usernameToken.addChildElement(name);
		usernameElement.setTextContent(username);
		
		name = envelope.createName("Password", "wsse", properties.getWsse());
		SOAPElement passwordElement = usernameToken.addChildElement(name);
		name = envelope.createName("Type");
		passwordElement.addAttribute(name, properties.getPasswordtype());
		passwordElement.setTextContent(password);
		message.saveChanges();
	}
	
}
