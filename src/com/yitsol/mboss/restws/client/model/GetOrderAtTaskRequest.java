
package com.yitsol.mboss.restws.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement
public class GetOrderAtTaskRequest {
	
	@XmlElement(name = "OrderID")
	private String orderId;
	
	@XmlElement(name = "AcceptedUserName")
	private String acceptedUserName;
	
	@XmlElement(name = "Task")
	private String task;
	
	public void setOrderId(String orderId) {
	
		this.orderId = orderId;
	}
	
	public void setAcceptedUserName(String acceptedUserName) {
	
		this.acceptedUserName = acceptedUserName;
	}
	
	public void setTask(String task) {
	
		this.task = task;
	}
}
