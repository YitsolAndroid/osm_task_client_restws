
package com.yitsol.mboss.restws.client.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement
@XmlType(propOrder = { "orderId", "acceptedUsername", "task", "executionMode", "accept", "states",
		"nameSpace", "version" })
public class GetNextOrderAtTask {
	
	@XmlElement(name = "OrderID")
	private String orderId;
	
	@XmlElement(name = "AcceptedUserName")
	private String acceptedUsername;
	
	@XmlElement(name = "Task")
	private String task;
	
	@XmlElement(name = "ExecutionMode")
	private String executionMode;
	
	@XmlElement(name = "Accept")
	private String accept;
	
	@XmlElement(name = "State")
	private List<String> states;
	
	@XmlElement(name = "Namespace")
	private String nameSpace;
	
	@XmlElement(name = "Version")
	private String version;
	
	public void setOrderId(String orderId) {
	
		this.orderId = orderId;
	}
	
	public void setAcceptedUsername(String acceptedUsername) {
	
		this.acceptedUsername = acceptedUsername;
	}
	
	public void setTask(String task) {
	
		this.task = task;
	}
	
	public void setExecutionMode(String executionMode) {
	
		this.executionMode = executionMode;
	}
	
	public void setAccept(String accept) {
	
		this.accept = accept;
	}
	
	public void setStates(List<String> states) {
	
		this.states = states;
	}
	
	public void setNameSpace(String nameSpace) {
	
		this.nameSpace = nameSpace;
	}
	
	public void setVersion(String version) {
	
		this.version = version;
	}
}
