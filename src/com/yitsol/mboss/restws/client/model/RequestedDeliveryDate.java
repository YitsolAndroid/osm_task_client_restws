
package com.yitsol.mboss.restws.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement(namespace = "com.blue.matrix.task.client.model.OrderId")
public class RequestedDeliveryDate {
	
	@XmlElement(name = "FromDate")
	public String fromDate;
	@XmlElement(name = "ToDate")
	public String toDate;
	
}
