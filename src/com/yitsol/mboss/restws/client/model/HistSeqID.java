
package com.yitsol.mboss.restws.client.model;

/**
 * 
 * @author jaya sankar
 *
 */

public class HistSeqID {
	
	private String histSeqId;
	
	public String getHistSeqId() {
	
		return histSeqId;
	}
	
	public void setHistSeqId(String histSeqId) {
	
		this.histSeqId = histSeqId;
	}
	
}
