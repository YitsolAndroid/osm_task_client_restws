
package com.yitsol.mboss.restws.client.model;

/**
 * 
 * @author jaya sankar
 *
 */

public class OrderStatus {
	
	private String id;
	
	private String orderstate;
	
	public String getOrderstate() {
	
		return orderstate;
	}
	
	public void setOrderstate(String orderstate) {
	
		this.orderstate = orderstate;
	}
	
	public String getId() {
	
		return id;
	}
	
	public void setId(String id) {
	
		this.id = id;
	}
	
}
