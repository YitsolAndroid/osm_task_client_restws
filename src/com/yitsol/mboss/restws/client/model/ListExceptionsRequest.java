
package com.yitsol.mboss.restws.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement
public class ListExceptionsRequest {
	
	@XmlElement(name = "OrderHistID")
	private String orderHistID;
	
	@XmlElement(name = "OrderId")
	private String orderId;
	
	public void setOrderHistID(String orderHistID) {
	
		this.orderHistID = orderHistID;
	}
	
	public void setOrderId(String orderId) {
	
		this.orderId = orderId;
	}
	
}
