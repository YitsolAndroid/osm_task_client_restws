
package com.yitsol.mboss.restws.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement
public class ListViewsRequest {
	
	@XmlElement(name = "OrderSource")
	private String orderSource;
	
	@XmlElement(name = "OrderType")
	private String orderType;
	
	@XmlElement(name = "Namespace")
	private String nameSpace;
	
	@XmlElement(name = "Version")
	private String version;
	
	public void setOrderSource(String orderSource) {
	
		this.orderSource = orderSource;
	}
	
	public void setOrderType(String orderType) {
	
		this.orderType = orderType;
	}
	
	public void setNameSpace(String nameSpace) {
	
		this.nameSpace = nameSpace;
	}
	
	public void setVersion(String version) {
	
		this.version = version;
	}
	
}
