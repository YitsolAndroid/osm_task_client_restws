
package com.yitsol.mboss.restws.client.model;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;

import org.apache.log4j.Logger;

import com.yitsol.mboss.restws.client.WebserviceHTTPClient;



public class TaskClientProperties {
	
	final static Logger logger = Logger.getLogger(WebserviceHTTPClient.class);
	private String URL;
	private String GET_ORDER_FILE;
	private String UPDATED_ORDER_FILE;
	private String UPDATED_NODES_FILE;
	private String DATA_CHANGE_FILE;
	private String ATTACHMENT_PATH;
	private String RESUME_ORDER;
	private String SUSPEND_ORDER;
	private String CANCEL_ORDER;
	private String FAIL_ORDER;
	private String FIND_ORDER;
	private String ABORT_ORDER;
	private String RESOLVE_ORDER;
	private String GET_ORDER_COMPENSATIONS;
	
	public String getGET_ORDER_COMPENSATIONS() {
	
		return GET_ORDER_COMPENSATIONS;
	}
	
	public String getGET_ORDER_PROCESS_HISTORY() {
	
		return GET_ORDER_PROCESS_HISTORY;
	}
	
	public String getGET_COMPENSATIONS_PLAN() {
	
		return GET_COMPENSATIONS_PLAN;
	}
	
	public static URL getEndpointDiagnostic() {
	
		return endpointDiagnostic;
	}
	
	private String GET_ORDER_PROCESS_HISTORY;
	private String GET_COMPENSATIONS_PLAN;
	private String URL_Diagnostic;
	private static URL endpoint;
	private static URL endpointDiagnostic;
	final private static String ws = "http://xmlns.oracle.com/communications/ordermanagement";
	private final static String wsse = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	final private static String wsu = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
	final private static String passwordType = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";
	final private static String soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
	
	public TaskClientProperties ( ) {
	
		Properties props = new Properties();
		try {
			props.load(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("/WebserviceClient.properties"));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		URL_Diagnostic = props.getProperty("weblogic.webservice.diagnostic.uri");
		URL = props.getProperty("weblogic.webservice.uri");
		GET_ORDER_FILE = props.getProperty("getOrder");
		UPDATED_ORDER_FILE = props.getProperty("updateOrderUsingUpdatedOrder");
		UPDATED_NODES_FILE = props.getProperty("updateOrderUsingUpdatedNodes");
		DATA_CHANGE_FILE = props.getProperty("updateOrderUsingDataChange");
		ATTACHMENT_PATH = props.getProperty("attachment");
		RESUME_ORDER = props.getProperty("resumeOrder");
		SUSPEND_ORDER = props.getProperty("suspendOrder");
		CANCEL_ORDER = props.getProperty("cancelOrder");
		ABORT_ORDER = props.getProperty("abortOrder");
		FAIL_ORDER = props.getProperty("failOrder");
		FIND_ORDER = props.getProperty("findOrder");
		RESOLVE_ORDER = props.getProperty("resolveFailure");
		GET_ORDER_COMPENSATIONS = props.getProperty("getOrderCompensationplan");
		GET_ORDER_PROCESS_HISTORY = props.getProperty("getOrderProcessHistory");
		GET_COMPENSATIONS_PLAN = props.getProperty("getCompensationplan");
		System.setProperty("javax.xml.soap.MessageFactory",
				"com.sun.xml.internal.messaging.saaj.soap.ver1_1.SOAPMessageFactory1_1Impl");
		System.setProperty("javax.xml.soap.SOAPConnectionFactory",
				"weblogic.wsee.saaj.SOAPConnectionFactoryImpl");
		
		try {
			endpoint = new URL(URL);
			endpointDiagnostic = new URL(URL_Diagnostic);
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
	
	public static Logger getLogger() {
	
		return logger;
	}
	
	public String getURL() {
	
		return URL;
	}
	
	public String getGET_ORDER_FILE() {
	
		return GET_ORDER_FILE;
	}
	
	public String getUPDATED_ORDER_FILE() {
	
		return UPDATED_ORDER_FILE;
	}
	
	public String getUPDATED_NODES_FILE() {
	
		return UPDATED_NODES_FILE;
	}
	
	public String getDATA_CHANGE_FILE() {
	
		return DATA_CHANGE_FILE;
	}
	
	public String getATTACHMENT_PATH() {
	
		return ATTACHMENT_PATH;
	}
	
	public String getRESUME_ORDER() {
	
		return RESUME_ORDER;
	}
	
	public String getSUSPEND_ORDER() {
	
		return SUSPEND_ORDER;
	}
	
	public String getCANCEL_ORDER() {
	
		return CANCEL_ORDER;
	}
	
	public String getFAIL_ORDER() {
	
		return FAIL_ORDER;
	}
	
	public String getFIND_ORDER() {
	
		return FIND_ORDER;
	}
	
	public String getABORT_ORDER() {
	
		return ABORT_ORDER;
	}
	
	public String getRESOLVE_ORDER() {
	
		return RESOLVE_ORDER;
	}
	
	public static URL getEndpoint() {
	
		return endpoint;
	}
	
	public static String getWs() {
	
		return ws;
	}
	
	public static String getWsse() {
	
		return wsse;
	}
	
	public static String getWsu() {
	
		return wsu;
	}
	
	public static String getPasswordtype() {
	
		return passwordType;
	}
	
	public static String getSoapenv() {
	
		return soapenv;
	}
	
}
