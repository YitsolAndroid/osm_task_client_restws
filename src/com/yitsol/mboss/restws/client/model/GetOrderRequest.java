
package com.yitsol.mboss.restws.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement
public class GetOrderRequest {
	
	@XmlElement(name = "OrderID")
	private String orderId;
	
	@XmlElement(name = "Accept")
	private String accept;
	
	@XmlElement(name = "OrderHistID")
	private String orderHistId;
	
	@XmlElement(name = "AcceptedUserName")
	private String acceptedName;
	
	public void setOrderId(String orderId) {
	
		this.orderId = orderId;
	}
	
	public void setAccept(String accept) {
	
		this.accept = accept;
	}
	
	public void setOrderHistId(String orderHistId) {
	
		this.orderHistId = orderHistId;
	}
	
	public void setAcceptedName(String acceptedName) {
	
		this.acceptedName = acceptedName;
	}
	
}
