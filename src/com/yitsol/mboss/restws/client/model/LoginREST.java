
package com.yitsol.mboss.restws.client.model;

/**
 * 
 * @author jaya sankar
 *
 */

public class LoginREST {
	
	private String status;
	private String key;
	
	public String getStatus() {
	
		return status;
	}
	
	public void setStatus(String status) {
	
		this.status = status;
	}
	
	public String getKey() {
	
		return key;
	}
	
	public void setKey(String key) {
	
		this.key = key;
	}
	
}
