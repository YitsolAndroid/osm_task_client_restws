
package com.yitsol.mboss.restws.client.model;

/**
 * 
 * @author jaya sankar
 *
 */

public class Status {
	
	private String process_status_mnemonic;
	
	public String getProcess_status_mnemonic() {
	
		return process_status_mnemonic;
	}
	
	public void setProcess_status_mnemonic(String process_status_mnemonic) {
	
		this.process_status_mnemonic = process_status_mnemonic;
	}
	
}
