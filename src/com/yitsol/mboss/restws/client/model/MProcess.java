
package com.yitsol.mboss.restws.client.model;

/**
 * 
 * @author jaya sankar
 *
 */

public class MProcess {
	
	private String processId;
	private String processIdDesc;
	
	public String getProcessId() {
	
		return processId;
	}
	
	public void setProcessId(String processId) {
	
		this.processId = processId;
	}
	
	public String getProcessIdDesc() {
	
		return processIdDesc;
	}
	
	public void setProcessIdDesc(String processIdDesc) {
	
		this.processIdDesc = processIdDesc;
	}
	
}
