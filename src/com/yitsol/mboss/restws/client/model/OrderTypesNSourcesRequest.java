
package com.yitsol.mboss.restws.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement
public class OrderTypesNSourcesRequest {
	
	@XmlElement(name = "Namespace")
	private String nameSpace;
	
	@XmlElement(name = "Version")
	private String version;
	
	public void setNameSpace(String nameSpace) {
	
		this.nameSpace = nameSpace;
	}
	
	public void setVersion(String version) {
	
		this.version = version;
	}
}
