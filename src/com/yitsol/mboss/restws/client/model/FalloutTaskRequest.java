
package com.yitsol.mboss.restws.client.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "orderId", "orderHistId", "fallout", "reason" })
@XmlRootElement()
public class FalloutTaskRequest {
	
	@XmlElement(name = "OrderID")
	private String orderId;
	
	@XmlElement(name = "OrderHistID")
	private String orderHistId;
	
	@XmlElement(name = "Fallout")
	private String fallout;
	
	@XmlElement(name = "Reason")
	private String reason;
	
	public String getOrderId() {
	
		return orderId;
	}
	
	public String getOrderHistId() {
	
		return orderHistId;
	}
	
	public String getFallout() {
	
		return fallout;
	}
	
	public String getReason() {
	
		return reason;
	}
	
	public void setOrderId(String orderId) {
	
		this.orderId = orderId;
	}
	
	public void setOrderHistId(String orderHistId) {
	
		this.orderHistId = orderHistId;
	}
	
	public void setFallout(String fallout) {
	
		this.fallout = fallout;
	}
	
	public void setReason(String reason) {
	
		this.reason = reason;
	}
	
}
