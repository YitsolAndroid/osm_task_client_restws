
package com.yitsol.mboss.restws.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement(namespace = "com.blue.matrix.task.client.model.Field")
public class Cartridge {
	
	@XmlElement(name = "Name")
	public String name;
	@XmlElement(name = "Version")
	public String version;
}