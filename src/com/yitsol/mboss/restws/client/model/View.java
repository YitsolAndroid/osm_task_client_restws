
package com.yitsol.mboss.restws.client.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement
public class View implements Serializable {
	
	private String viewName;
	
	public String getViewName() {
	
		return viewName;
	}
	
	public void setViewName(String viewName) {
	
		this.viewName = viewName;
	}
	
}
