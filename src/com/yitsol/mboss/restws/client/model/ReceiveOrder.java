/**
 * 
 * @author Sahil Nokhwal
 *
 */

package com.yitsol.mboss.restws.client.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "orderId", "orderHistId" })
@XmlRootElement()
public class ReceiveOrder {
	
	@XmlElement(name = "OrderID")
	private String orderId;
	
	@XmlElement(name = "OrderHistID")
	private String orderHistId;
	
	public String getOrderId() {
	
		return orderId;
	}
	
	public String getOrderHistId() {
	
		return orderHistId;
	}
	
	public void setOrderId(String orderId) {
	
		this.orderId = orderId;
	}
	
	public void setOrderHistId(String orderHistId) {
	
		this.orderHistId = orderHistId;
	}
	
}
