
package com.yitsol.mboss.restws.client.dynamicsql;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author jaya sankar
 *
 */

public class Table {
	
	private final String name;
	private final String alias;
	private List<String> columnsWithAlias = new ArrayList<String>();
	private List<String> groupFunctions = new ArrayList<String>();
	
	public Table (String name , String alias ) {
	
		this.name = name;
		this.alias = alias;
	}
	
	public String getName() {
	
		return name;
	}
	
	public String getAlias() {
	
		return alias;
	}
	
	public void addColumnsToSelect(List<String> s) {
	
		columnsWithAlias.add(alias + "." + s);
	}
	
	public List<String> getColumnsWithAlias() {
	
		return columnsWithAlias;
	}
	
	public void addGroupFunctions(List<String> sGroup) {
	
		groupFunctions.addAll(sGroup);
	}
	
	public List<String> getGroupFunctions() {
	
		return groupFunctions;
	}
	
}
