
package com.yitsol.mboss.restws.client.dynamicsql;

/**
 * 
 * @author jaya sankar
 *
 */

public class SearchCriteria {
	
	private String value;
	private String table1Colum1;
	private String opertor;
	private Object values;
	
	public String getValue() {
	
		return value;
	}
	
	public void setValue(String value) {
	
		this.value = value;
	}
	
	private Table table1;
	
	public Table getTable1() {
	
		return table1;
	}
	
	public void setTable1(Table table1) {
	
		this.table1 = table1;
	}
	
	public String getTable1Colum1() {
	
		return table1Colum1;
	}
	
	public void setTable1Colum1(String table1Colum1) {
	
		this.table1Colum1 = table1Colum1;
	}
	
	public String getOpertor() {
	
		return opertor;
	}
	
	public void setOpertor(String opertor) {
	
		this.opertor = opertor;
	}
	
	public Object getValues() {
	
		return values;
	}
	
	public void setValues(Object values) {
	
		this.values = values;
	}
	
}
