package com.yitsol.mboss.restws.xmlapi;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.yitsol.mboss.restws.client.model.LoginREST;
import com.yitsol.mboss.restws.client.xml.XmlAPIClient;
import com.yitsol.mboss.restws.exception.CredentialsException;
import com.yitsol.mboss.restws.util.RestfulJsonUtil;

@Path("/xml_api")
public class XmlAPIRestFul {
	
	private static final Logger logger = Logger.getLogger(XmlAPIRestFul.class);
	private String username;
	private String password;
	private String requestXml;
	private String publicIP;
	private String port;
	private JSONObject jsonOutputObject;
	private JSONObject jsonInputObject;
	private String outputMessageAsString;
	
	@Context
	ContainerRequestContext request;
	
	private void setter(JSONObject jsonInputObject)
	
	{
	
		try {
			logger.info("setter method started........");
			username = (String) request.getProperty("username");
			password = (String) request.getProperty("password");
			publicIP = jsonInputObject.getString("publicIP");
			port = jsonInputObject.getString("port");
			
		}
		catch (JSONException e) {
			logger.error(e);
		}
	}
	
	@POST
	@Path("/login")
	@Produces("application/json")
	@Consumes("application/json")
	public Response login(InputStream incommingJsonObj)
	
	{
	
		try {
			
			logger.info("app login started............");
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			
			String result = null;
			try {
				result = client.getLoginData(publicIP, port, username, password);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			LoginREST loginStatus = new LoginREST();
			
			if (result.equals("failure")) {
				
				loginStatus.setStatus("1");
				loginStatus.setKey("failure");
				return Response.ok(loginStatus, MediaType.APPLICATION_JSON).build();
			}
			else if (result.equals("wport")) {
				loginStatus.setStatus("2");
				loginStatus.setKey("wport");
				return Response.ok(loginStatus, MediaType.APPLICATION_JSON).build();
			}
			else {
				
				loginStatus.setStatus("0");
				loginStatus.setKey(result);
				return Response.ok(loginStatus, MediaType.APPLICATION_JSON).build();
			}
			
		}
		catch (JSONException e) {
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	/*@POST
	@Path("/logout")
	@Produces("application/json")
	@Consumes("application/json")
	public Response logout(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			
			String result = client.logOut(publicIP, port, username, password);
			LoginREST loginStatus = new LoginREST();
			
			if (result == "failure") {
				
				loginStatus.setStatus("1");
				loginStatus.setKey("failure");
				
				return Response.ok(loginStatus, MediaType.APPLICATION_JSON).build();
			}
			else {
				
				loginStatus.setStatus("0");
				loginStatus.setKey("success");
				return Response.ok(loginStatus, MediaType.APPLICATION_JSON).build();
			}
			
		}
		catch (JSONException e) {
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/taskDescription")
	@Produces("application/json")
	@Consumes("application/json")
	public Response taskDescription(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			TaskDescription ro = new TaskDescription();
			ro.setNamespace(jsonInputObject.getString("namespace"));
			ro.setVersion(jsonInputObject.getString("version"));
			
			String result = client.taskDescription(publicIP, port, username, password, ro);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
			
		}
		catch (JSONException e) {
			
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/setException")
	@Produces("application/json")
	@Consumes("application/json")
	public Response setException(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			SetException ro = new SetException();
			ro.setOrderId(jsonInputObject.getString("orderId"));
			ro.setOrderHistId(jsonInputObject.getString("orderHistID"));
			ro.setStatus(jsonInputObject.getString("status"));
			
			String result = client.setException(publicIP, port, username, password, ro);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
			
		}
		catch (JSONException e) {
			
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/receiveOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public Response receiveOrder(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			ReceiveOrder ro = new ReceiveOrder();
			ro.setOrderId(jsonInputObject.getString("orderId"));
			ro.setOrderHistId(jsonInputObject.getString("orderHistID"));
			
			String result = client.receiveOrder(publicIP, port, username, password, ro);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
			
		}
		catch (JSONException e) {
			
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/getnextorderattask")
	@Produces("application/json")
	@Consumes("application/json")
	public Response getNextOrderAtTask(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			GetNextOrderAtTask task = nextOrderAtTaskBuilder();
			
			String result = client.getNextOrderAtTask(publicIP, port, username, password, task);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
			
		}
		catch (JSONException e) {
			
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	*//**
	 * @return
	 * @throws JSONException
	 *//*
	private GetNextOrderAtTask nextOrderAtTaskBuilder() throws JSONException {
	
		GetNextOrderAtTask task = new GetNextOrderAtTask();
		task.setOrderId(jsonInputObject.getString("orderId"));
		task.setAccept(jsonInputObject.getString("accept"));
		task.setAcceptedUsername(jsonInputObject.getString("acceptedusername"));
		task.setExecutionMode(jsonInputObject.getString("exceutionmode"));
		task.setNameSpace(jsonInputObject.getString("namespace"));
		task.setTask(jsonInputObject.getString("task"));
		task.setVersion(jsonInputObject.getString("version"));
		List<String> states = (List<String>) new ArrayList();
		states.add(jsonInputObject.getString("state1"));
		states.add(jsonInputObject.getString("state2"));
		states.add(jsonInputObject.getString("state3"));
		task.setStates(states);
		return task;
	}
	
	@POST
	@Path("/fallOutTask")
	@Produces("application/json")
	@Consumes("application/json")
	public Response fallOutTask(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			FalloutTaskRequest fall = new FalloutTaskRequest();
			fall.setFallout(jsonInputObject.getString("fallout"));
			fall.setOrderHistId(jsonInputObject.getString("orderhistId"));
			fall.setOrderId(jsonInputObject.getString("orderId"));
			fall.setReason(jsonInputObject.getString("reason"));
			String result = client.fallOutTask(publicIP, port, username, password, fall);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
			
		}
		catch (JSONException e) {
			
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/getorderattask")
	@Produces("application/json")
	@Consumes("application/json")
	public Response getOrderAtTask(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			GetOrderAtTaskRequest order = new GetOrderAtTaskRequest();
			order.setTask(jsonInputObject.getString("task"));
			order.setOrderId(jsonInputObject.getString("orderId"));
			order.setAcceptedUserName(jsonInputObject.getString("AcceptedUserName"));
			String result = client.getOrderAtTask(publicIP, port, username, password, order);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
			
		}
		catch (JSONException e) {
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/getorderdatahistory")
	@Produces("application/json")
	@Consumes("application/json")
	public Response getOrderDataHistory(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			GetOrderDataHistoryRequest order = new GetOrderDataHistoryRequest();
			order.setOrderId(jsonInputObject.getString("orderId"));
			order.setViewId(jsonInputObject.getString("view"));
			String result = client.getOrderDataHistory(publicIP, port, username, password, order);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
		}
		catch (JSONException e) {
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/getOrderProcessHistory")
	@Produces("application/json")
	@Consumes("application/json")
	public Response getOrderProcessHistory(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			GetOrderProcessHistoryRequest order = new GetOrderProcessHistoryRequest();
			order.setOrderId(jsonInputObject.getString("orderId"));
			String result = client
					.getOrderProcessHistory(publicIP, port, username, password, order);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
		}
		catch (JSONException e) {
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/getOrderStateHistory")
	@Produces("application/json")
	@Consumes("application/json")
	public Response getOrderStateHistory(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			GetOrderStateHistoryRequest order = new GetOrderStateHistoryRequest();
			order.orderId = jsonInputObject.getString("orderId");
			String result = client.getOrderStateHistory(publicIP, port, username, password, order);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
		}
		catch (JSONException e) {
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/getTaskStatuses")
	@Produces("application/json")
	@Consumes("application/json")
	public Response getTaskStatuses(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			GetTaskStatusesRequest task = new GetTaskStatusesRequest();
			task.setNamePsace(jsonInputObject.getString("nameSpace"));
			task.setTask(jsonInputObject.getString("task"));
			task.setVersion(jsonInputObject.getString("version"));
			String result = client.getTaskStatuses(publicIP, port, username, password, task);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
		}
		catch (JSONException e) {
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/getUserInfo")
	@Produces("application/json")
	@Consumes("application/json")
	public Response getUserInfo(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			GetUserInfoRequest user = new GetUserInfoRequest();
			String result = client.getUserInfo(publicIP, port, username, password, user);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
		}
		catch (JSONException e) {
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/listExceptions")
	@Produces("application/json")
	@Consumes("application/json")
	public Response listExceptions(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			ListExceptionsRequest exceptions = new ListExceptionsRequest();
			exceptions.setOrderHistID(jsonInputObject.getString("orderHistId"));
			exceptions.setOrderId(jsonInputObject.getString("orderId"));
			String result = client.listExceptions(publicIP, port, username, password, exceptions);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
		}
		catch (JSONException e) {
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/listViews")
	@Produces("application/json")
	@Consumes("application/json")
	public Response listViews(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			ListViewsRequest views = new ListViewsRequest();
			views.setNameSpace(jsonInputObject.getString("nameSpace"));
			views.setOrderSource(jsonInputObject.getString("orderSource"));
			views.setOrderType(jsonInputObject.getString("orderType"));
			views.setVersion(jsonInputObject.getString("version"));
			String result = client.listViews(publicIP, port, username, password, views);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
		}
		catch (JSONException e) {
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/notifications")
	@Produces("application/json")
	@Consumes("application/json")
	public Response notifications(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			NotificationsRequest notification = new NotificationsRequest();
			String result = client.notifications(publicIP, port, username, password, notification);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
		}
		catch (JSONException e) {
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/orderTypesNSources")
	@Produces("application/json")
	@Consumes("application/json")
	public Response orderTypesNSources(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			OrderTypesNSourcesRequest order = new OrderTypesNSourcesRequest();
			order.setNameSpace(jsonInputObject.getString("namespace"));
			order.setVersion(jsonInputObject.getString("version"));
			String result = client.orderTypesNSources(publicIP, port, username, password, order);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
		}
		catch (JSONException e) {
			
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/modifyRemark")
	@Produces("application/json")
	@Consumes("application/json")
	public Response modifyRemark(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			String orderId = jsonInputObject.getString("orderId");
			String task = jsonInputObject.getString("task");
			
			DB_Connection db = new DB_Connection();
			HistSeqID hist = db.getHistSeqID(orderId, task);
			XmlAPIClient client = new XmlAPIClient();
			ModifyRemark remark = new ModifyRemark();
			remark.setOrderId(orderId);
			remark.setOrderHistId(hist.getHistSeqId());
			remark.setRemarkId(jsonInputObject.getString("remarkId"));
			remark.setText(jsonInputObject.getString("text"));
			String result = client.modifyRemark(publicIP, port, username, password, remark);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
			
		}
		catch (JSONException e) {
			
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/queryString")
	@Produces("application/json")
	@Consumes("application/json")
	public Response queryString(InputStream incommingJsonObj) {
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			String inputString = queryString();
			
			XmlAPIClient client = new XmlAPIClient();
			
			String result = client.queryRequest(publicIP, port, username, password, inputString);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
		}
		catch (JSONException e) {
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Please check the Json data\"}");
			
		}
		
	}
	
	private String queryString() throws JSONException {
	
		String executionModeString;
		String nameSpaceString;
		String orderIdString;
		String orderSourceString;
		String orderStateString;
		String orderTypeString;
		String referenceString;
		String stateString;
		String taskString;
		String userString;
		String dateString;
		String statusString;
		
		String exceutionMode = jsonInputObject.getString("executionmode");
		String nameSpace = jsonInputObject.getString("namespace");
		String orderId = jsonInputObject.getString("orderId");
		String orderSource = jsonInputObject.getString("orderSource");
		String orderState = jsonInputObject.getString("orderState");
		String orderType = jsonInputObject.getString("orderType");
		String reference = jsonInputObject.getString("reference");
		String state = jsonInputObject.getString("state");
		String task = jsonInputObject.getString("task");
		String user = jsonInputObject.getString("user");
		String status = jsonInputObject.getString("status");
		String from = jsonInputObject.getString("createDateFrom");
		String to = jsonInputObject.getString("createDateTo");
		String version = jsonInputObject.getString("version");
		String start = "<Query.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">";
		String end = "</Query.Request>";
		String data = start;
		
		if (!exceutionMode.isEmpty()) {
			executionModeString = "<ExecutionMode>" + exceutionMode + "</ExecutionMode>";
			data = data + executionModeString;
		}
		if (!nameSpace.isEmpty()) {
			nameSpaceString = "<Namespace>" + nameSpace + "</Namespace>";
			data = data + nameSpaceString;
		}
		if (!orderId.isEmpty()) {
			orderIdString = "<OrderID>" + orderId + "</OrderID>";
			data = data + orderIdString;
		}
		if (!orderSource.isEmpty()) {
			orderSourceString = "<OrderSource>" + orderSource + "</OrderSource>";
			data = data + orderSourceString;
		}
		if (!orderState.isEmpty()) {
			
			orderStateString = "<OrderState>" + orderState + "</OrderState>";
			data = data + orderStateString;
		}
		if (!orderType.isEmpty()) {
			orderTypeString = "<OrderType>" + orderType + "</OrderType>";
			data = data + orderTypeString;
		}
		if (!reference.isEmpty()) {
			referenceString = "<Reference>" + reference + "</Reference>";
			data = data + referenceString;
		}
		if (!state.isEmpty()) {
			stateString = "<State>" + state + "</State>";
			data = data + stateString;
		}
		if (!task.isEmpty()) {
			taskString = "<Task>" + task + "</Task>";
			data = data + taskString;
		}
		if (!user.isEmpty()) {
			userString = "<User>" + user + "</User>";
			data = data + userString;
		}
		if (!status.isEmpty()) {
			statusString = "<Status>" + status + "</Status>";
			data = data + statusString;
		}
		if (!from.isEmpty() && !to.isEmpty()) {
			dateString = "<CreatedDate from=\"" + from + "\" to=\"" + to + "\" />";
			data = data + dateString;
		}
		if (!version.isEmpty()) {
			String versionString = "<Version>" + version + "</Version>";
			data = data + versionString;
			
		}
		String inputString = data + end;
		return inputString;
	}
	
	@POST
	@Path("/copyOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public Response copyOrder(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			CopyOrder copyOrder = new CopyOrder();
			copyOrder.setNameSpace(jsonInputObject.getString("namespace"));
			copyOrder.setOrderSources(jsonInputObject.getString("sources"));
			copyOrder.setOrderType(jsonInputObject.getString("type"));
			copyOrder.setOriginalOrderId(jsonInputObject.getString("orderId"));
			copyOrder.setPriority(jsonInputObject.getString("priority"));
			copyOrder.setReference(jsonInputObject.getString("reference"));
			copyOrder.setVersion(jsonInputObject.getString("version"));
			String result = client.copyOrder(publicIP, port, username, password, copyOrder);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
			
		}
		catch (JSONException e) {
			
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}
	
	@POST
	@Path("/createOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public Response createOrder(InputStream incommingJsonObj)
	
	{
	
		CreateOrder createOrder = new CreateOrder();
		createOrder.setNamespace("xyx");
		createOrder.setOrderSource("sdcfsd");
		createOrder.setOrderType("type");
		createOrder.setPriority("high");
		createOrder.setReference("ref");
		createOrder.setVersion("version");
		
		StringWriter w = new StringWriter();
		JAXBElement<CreateOrder> element = new JAXBElement<CreateOrder>(new QName(
				"urn:com:metasolv:oms:xmlapi:1", "CreateOrder.Request"), CreateOrder.class,
				createOrder);
		
		try {
			final Marshaller m = JAXBContext.newInstance(CreateOrder.class).createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.marshal(element, w);
		}
		catch (JAXBException e1) {
			logger.error(e1);
		}
		
		return Response.ok(RestfulJsonUtil.checkSoapMessage(w.toString()),
				MediaType.APPLICATION_JSON).build();
	}
	
	@POST
	@Path("/updateOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public Response updateOrder(InputStream incommingJsonObj)
	
	{
	
		String updateOrder = null;
		
		try {
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			updateOrder = updateOrderBuilder();
			XmlAPIClient client = new XmlAPIClient();
			String result = client.updateOrder(publicIP, port, username, password, updateOrder);
			
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
		}
		catch (JSONException e) {
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
		}
		catch (Exception e) {
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Execption Occured,Please check the Json data\"}");
		}
		
	}

	*//**
	 * @return
	 * @throws JSONException
	 *//*
	private String updateOrderBuilder() throws JSONException {
	
		String updateOrder;
		DB_Connection db = new DB_Connection();
		String orderId = jsonInputObject.getString("orderId");
		View view = db.getView(orderId);
		updateOrder = "<UpdateOrder.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">"
				+ "<OrderID>" + jsonInputObject.getString("orderId") + "</OrderID><View>"
				+ view.getViewName() + "</View><UpdatedNodes>"
				+ jsonInputObject.getString("root") + "</UpdatedNodes></UpdateOrder.Request>";
		return updateOrder;
	}
	
	@POST
	@Path("/addRemark")
	@Produces("application/json")
	@Consumes("application/json")
	public Response addRemark(InputStream incommingJsonObj)
	
	{
	
		try {
			
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			setter(jsonInputObject);
			XmlAPIClient client = new XmlAPIClient();
			DB_Connection db = new DB_Connection();
			String orderId = jsonInputObject.getString("orderId");
			View view = db.getView(orderId);
			String request1 = "<UpdateOrder.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">";
			String request2 = "<OrderID>" + orderId + "</OrderID>";
			String request3 = "<View>" + view.getViewName() + "</View>";
			String request4 = "<AddRemark> <Text>" + jsonInputObject.getString("remark")
					+ "</Text>";
			String request5 = "<Attachments>" + jsonInputObject.getString("attachment")
					+ "</Attachments></AddRemark></UpdateOrder.Request>";
			String xml = request1 + request2 + request3 + request4 + request5;
			String result = client.addRemark(publicIP, port, username, password, xml);
			return Response
					.ok(RestfulJsonUtil.checkSoapMessage(result), MediaType.APPLICATION_JSON)
					.build();
			
		}
		catch (JSONException e) {
			
			logger.error(e);
			throw new CredentialsException(
					"{\"status\":400,\"errorMsg\":\"Json Execption Occured,Please check the Json data\"}");
			
		}
		
	}*/
	
}