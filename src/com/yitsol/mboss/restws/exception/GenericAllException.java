
package com.yitsol.mboss.restws.exception;

import java.io.Serializable;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * 
 * @author jaya sankar
 *
 */

public class GenericAllException extends WebApplicationException {
	
	public GenericAllException ( ) {
	
		super();
		
	}
	
	public GenericAllException (String message ) {
	
		super(message);
	}
	
}
